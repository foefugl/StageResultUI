using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HonoHime.ScoreRankSystem;
using System;
using HonoHime.ScoreRankSystem.StageEvaluate;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class StageResultManager : MonoBehaviour
{
    private static StageResultManager ins;
    public static StageResultManager Ins => ins;
    public StageEvaluateDataDict stageEvaluateDatas;
    public StageResultUIHandler uIHandler;
    [SerializeField] StageData stageData;

    public bool enableCancelListen = true;

    [ReadOnly]
    public StageResultReport currentReport;

    [ReadOnly]
    public bool isUsed;


    [FoldoutGroup("Event")]
    public UnityEvent OnUseEvent;
    [FoldoutGroup("Event")]
    public UnityEvent OnFinishEvent;

    [System.Serializable]
    public class StageEvaluateDataDict : UnitySerializedDictionary<StageData.Category, StageEvaluateData> { }


    [Button]
    public void Use()
    {
        if (isUsed) return;

        //StageData stageData = StageManager.ins.lastFinishStageData;
        if (stageData == null) return;

        currentReport = GenerateReport(stageData);

        isUsed = true;
        uIHandler.Show(currentReport);

        OnUseEvent?.Invoke();
    }

    [Button]
    public void Finish()
    {
        if (!isUsed) return;
        isUsed = false;
        uIHandler.Hide();

        OnFinishEvent?.Invoke();
    }



    public static StageResultReport GenerateReport(StageData stageData)
    {
        StageResultReport report = new StageResultReport();

        report.stageName = stageData.stageName;

        //Debug.Log("stageData.endTime : " + stageData.endTime.ToString("HH:mm:ss") + " " + "stageData.startTime : " + stageData.startTime.ToString("HH:mm:ss"));

        TimeSpan timeDifference = stageData.endTime - stageData.startTime;
        report.gameTime = string.Format("{0:D2}:{1:D2}:{2:D2}", timeDifference.Hours, timeDifference.Minutes, timeDifference.Seconds);
        

        report.continueTime = stageData.continueTime;
        report.totalScore = Calculate(stageData.skirmishReports);
        report.finalRank = GetScoreRank(stageData, report.totalScore);
        report.skirmishReports = stageData.skirmishReports;

        return report;
    }

    private static int Calculate(List<StageData.SkirmishReport> _skirmishReports)
    {
        int totalScore = 0;

        for (int i = 0; i < _skirmishReports.Count; i++)
        {
            totalScore += _skirmishReports[i].scoreReport.totalScore;
        }

        return totalScore;
    }

    private static ScoreRank GetScoreRank(StageData stageData, int totalScore)
    {
        //StageEvaluateData stageEvaluateData = stageEvaluateDatas[category];

        int sScore = 0, aScore = 0, bScore = 0, cScore = 0;

        //�`�M�ŶZ����
        for (int i = 0; i < stageData.skirmishReports.Count; i++)
        {
            sScore += stageData.skirmishReports[i].evaluateData.battleScoreRank.S.Score;
            aScore += stageData.skirmishReports[i].evaluateData.battleScoreRank.A.Score;
            bScore += stageData.skirmishReports[i].evaluateData.battleScoreRank.B.Score;
            cScore += stageData.skirmishReports[i].evaluateData.battleScoreRank.C.Score;
        }

        if (totalScore > sScore)
            return ScoreRank.S;

        if (totalScore > aScore)
            return ScoreRank.A;

        if (totalScore > bScore)
            return ScoreRank.B;

        if (totalScore > cScore)
            return ScoreRank.C;

        return ScoreRank.D;
    }

    public struct StageResultReport
    {
        public string stageName;
        public string gameTime;
        public int continueTime;
        public int totalScore;
        public ScoreRank finalRank;
        public List<StageData.SkirmishReport> skirmishReports;
    }
}
