using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HonoHime.ScoreRankSystem.StageEvaluate
{
    [CreateAssetMenu(fileName = "StageEvaluateData", menuName = "HonoHime/Level/StageEvaluateData")]

    public class StageEvaluateData : ScriptableObject
    {
        public ScoreRankInfos battleScoreRank;
        public PerformanceInfo performanceInfo;
    }
    [System.Serializable]
    public class PerformanceInfo
    {
        public int deathCount;
        public float playTime;
        public float totalDamaged;
    }
}
