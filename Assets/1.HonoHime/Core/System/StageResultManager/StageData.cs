using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using HonoHime.ScoreRankSystem;
using System;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "StageData", menuName = "HonoHime/Level/StageData")]
[System.Serializable]
public class StageData : ScriptableObject
{
    [System.Serializable]
    public enum Category
    {
        Level1,
        Level2,
        Level3,
        Level4,
        Level5,
        Others,
        HubWorld,
        None,
        Level0
    }

    [FoldoutGroup("把计砞竚")]
    public List<SceneReference> constainScenes;

    [FoldoutGroup("把计砞竚")]
    public Category stageCategory;
    [FoldoutGroup("把计砞竚")]
    public string stageName;
    [FoldoutGroup("把计砞竚")]
    [ReadOnly]
    public string currentSceneName;
    [FoldoutGroup("把计砞竚")]
    [ReadOnly]
    public string currentScenePath;
    [FoldoutGroup("把计砞竚")]
    [ReadOnly]
    public DateTime startTime;
    [FoldoutGroup("把计砞竚")]
    [ReadOnly]
    public DateTime endTime;
    [FoldoutGroup("把计砞竚")]
    [ReadOnly]
    public int continueTime;


    [FoldoutGroup("把计砞竚")]
    public List<SkirmishReport> skirmishReports;

    [FoldoutGroup("把计砞竚")]
    public CustomDataDict customDataDict;


    #region Class
    [System.Serializable]
    public class CustomDataDict : UnitySerializedDictionary<string, CustomData> { }

    [System.Serializable]
    public class SkirmishReport
    {
        public BattleEvaluateData evaluateData;
        public ScoreReport scoreReport;
        public bool isPass;
    }

    [System.Serializable]
    public class CustomData
    {
        public enum Type
        {
            Bool,
            String,
            Int,
            Float
        }

        public Type type;

        
        [ShowIf("type", Type.Bool)]
        [ReadOnly]
        public bool boolValue;
        [ShowIf("type", Type.String)]
        [ReadOnly]
        public string stringValue;
        [ShowIf("type", Type.Int)]
        [ReadOnly]
        public int intValue;
        [ShowIf("type", Type.Float)]
        [ReadOnly]
        public float floatValue;
    }

    #endregion
}
