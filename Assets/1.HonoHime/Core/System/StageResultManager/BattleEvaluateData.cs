using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace HonoHime.ScoreRankSystem
{
    [CreateAssetMenu(fileName = "BattleEvaluateData", menuName = "HonoHime/Level/BattleEvaluateData")]
    public class BattleEvaluateData : ScriptableObject
    {
        public string ID;
        public TotalScoreRankInfos totalScoreRank;
        public ScoreRankInfos battleScoreRank;
        public RankClearTimeSetting clearTimeRank;
        public GetDamageCountRankSetting getDamageCountRankSetting;
    }
}