using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using HonoHime.ScoreRankSystem;

public class Skirmish : MonoBehaviour
{
    [EnumPaging]
    public ScoreRank scoreRank;
    public string skirmishName;
    public int skirmishScore;
    public bool isPass;

    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteS;
    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteA;
    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteB;
    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteC;
    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteD;
    [FoldoutGroup("物件設置")]
    public Image rankIcon;
    [FoldoutGroup("物件設置")]
    public Text nameText;
    [FoldoutGroup("物件設置")]
    public Text scoreText;


    public void SetSkirmish(string _skirmishName, int _skirmishScore, ScoreRank _scoreRank, bool _isPass)
    {
        skirmishName = _skirmishName;
        skirmishScore = _skirmishScore;
        scoreRank = _scoreRank;
        isPass = _isPass;

        UpdateUI();
    }



    private void UpdateUI()
    {
        if (isPass)
        {
            switch (scoreRank)
            {
                case ScoreRank.S:
                    rankIcon.sprite = spriteS;
                    break;

                case ScoreRank.A:
                    rankIcon.sprite = spriteA;
                    break;

                case ScoreRank.B:
                    rankIcon.sprite = spriteB;
                    break;

                case ScoreRank.C:
                    rankIcon.sprite = spriteC;
                    break;

                case ScoreRank.D:
                    rankIcon.sprite = spriteD;
                    break;
            }
            rankIcon.SetNativeSize();

            scoreText.text = skirmishScore.ToString("N0");
        }

        rankIcon.gameObject.SetActive(isPass);
        scoreText.gameObject.SetActive(isPass);

        nameText.text = skirmishName;
    }
}
