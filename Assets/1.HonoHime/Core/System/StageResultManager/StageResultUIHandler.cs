using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;
using UnityEngine.UI;

public class StageResultUIHandler : MonoBehaviour
{
    [FoldoutGroup("物件設定")]
    public StageResult stageResult;
    [FoldoutGroup("物件設定")]
    public Skirmish skirmishTemplate;
    [FoldoutGroup("物件設定")]
    public StagePlayerStat playerStatTemplate;
    [FoldoutGroup("物件設定")]
    public StageResultMovement resultMovement;

    public StageResultManager.StageResultReport resultReport;
    public List<Skirmish> currentSkirmish;
    public List<StagePlayerStat> currentPlayerStat;

    [Button]
    public void Show(StageResultManager.StageResultReport _resultReport)
    {
        resultReport = _resultReport;

        SetPlayerStat(resultReport);
        SetSkirmish(resultReport.skirmishReports);
        stageResult.SetResult(resultReport.stageName, resultReport.totalScore, resultReport.finalRank);

        Show();
    }

    [Button]
    public void Show()
    {
        resultMovement.Use();
    }

    [Button]
    public void Hide()
    {
        resultMovement.Finish();
    }


    private void SetSkirmish(List<StageData.SkirmishReport> skirmishReports)
    {
        ClearSkirmish();

        for (int i = 0; i < skirmishReports.Count; i++)
        {
            Skirmish skirmish = Instantiate(skirmishTemplate, skirmishTemplate.transform.parent);
            skirmish.SetSkirmish(skirmishReports[i].evaluateData.ID, skirmishReports[i].scoreReport.totalScore, skirmishReports[i].scoreReport.totalScoreRank, skirmishReports[i].isPass);
            skirmish.gameObject.SetActive(true);

            currentSkirmish.Add(skirmish);
        }
    }

    private void ClearSkirmish()
    {
        for (int i = 0; i < currentSkirmish.Count; i++)
        {
            Destroy(currentSkirmish[i].gameObject);
        }
    }

    private void SetPlayerStat(StageResultManager.StageResultReport _resultReport)
    {
        //繼續
        StagePlayerStat playerStat_Continue = Instantiate(playerStatTemplate, playerStatTemplate.transform.parent);
        playerStat_Continue.SetStat("繼續", _resultReport.continueTime.ToString());
        playerStat_Continue.gameObject.SetActive(true);
        currentPlayerStat.Add(playerStat_Continue);

        //通關時間
        StagePlayerStat playerStat_GameTime = Instantiate(playerStatTemplate, playerStatTemplate.transform.parent);
        playerStat_GameTime.SetStat("通關時間", _resultReport.gameTime);
        playerStat_GameTime.gameObject.SetActive(true);
        currentPlayerStat.Add(playerStat_GameTime);
    }

}
