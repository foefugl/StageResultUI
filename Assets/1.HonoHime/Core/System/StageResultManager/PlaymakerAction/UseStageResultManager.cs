using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;

[ActionCategory("StageResultManager")]

public class UseStageResultManager : FsmStateAction
{
    public StageResultManager stageResultManager;
    public bool lockPlayerAndHideHUD = true;
    public FsmEvent endEvent;

    public override void OnEnter()
    {
        base.OnEnter();

        if (stageResultManager == null)
        {
            Finish();
            return;
        }

        stageResultManager.OnFinishEvent.AddListener(DoFinish);
        stageResultManager.Use();
    }

    public void DoFinish()
    {
        stageResultManager.OnFinishEvent.RemoveListener(DoFinish);
        Fsm.Event(endEvent);
    }

}
