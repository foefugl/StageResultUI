using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class SkirmishMovement : MonoBehaviour
{
    [FoldoutGroup("����]�m")]
    public MMFeedbacks showFeedback;

    private void Awake()
    {
        Finish();
    }

    private void OnDisable()
    {
        Finish();
    }


    [Button]
    public void Use()
    {
        gameObject.SetActive(true);
        showFeedback.PlayFeedbacks();
    }

    [Button]
    public void Finish()
    {
        if(gameObject.activeSelf)
            gameObject.SetActive(false);
    }

}
