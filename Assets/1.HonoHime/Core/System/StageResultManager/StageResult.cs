using HonoHime.ScoreRankSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class StageResult : MonoBehaviour
{
    [EnumPaging]
    public ScoreRank scoreRank;
    public string stageName;
    public int totalScore;


    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteS;
    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteA;
    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteB;
    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteC;
    [FoldoutGroup("物件設置/IconAsset")]
    public Sprite spriteD;
    [FoldoutGroup("物件設置")]
    public Image rankIcon;
    [FoldoutGroup("物件設置")]
    public Text stageNameText;
    [FoldoutGroup("物件設置")]
    public Text totalScoreText;

    [Button]
    public void SetResult(string stageName, int totalScore, ScoreRank scoreRank)
    {
        this.stageName = stageName;
        this.totalScore = totalScore;
        this.scoreRank = scoreRank;
        UpdateUI();
    }

    [Button]
    public void UpdateUI()
    {
        switch (scoreRank)
        {
            case ScoreRank.S:
                rankIcon.sprite = spriteS;
                break;

            case ScoreRank.A:
                rankIcon.sprite = spriteA;
                break;

            case ScoreRank.B:
                rankIcon.sprite = spriteB;
                break;

            case ScoreRank.C:
                rankIcon.sprite = spriteC;
                break;

            case ScoreRank.D:
                rankIcon.sprite = spriteD;
                break;
        }
        rankIcon.SetNativeSize();

        stageNameText.text = stageName;
        totalScoreText.text = totalScore.ToString("N0");
    }

}
