using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StagePlayerStat : MonoBehaviour
{
    public enum Stat
    {
        Continue,
        GameTime
    }

    
    public Stat stat;
    public string statName;
    public string statValue;

    public Text statNameText;
    public Text statValueText;

    
    public void SetStat(string _statName, string _statValue)
    {
        statName = _statName;
        statValue = _statValue;

        statNameText.text = statName;
        statValueText.text = _statValue;
    }

}
