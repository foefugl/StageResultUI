using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;
using Sirenix.OdinInspector;
using DG.Tweening;

public class StageResultMovement : MonoBehaviour
{
    public CanvasGroup mainPanelGroup;
    public Transform skirmishGroups;
    public Transform playerStatGroups;
    public Transform finalScore;

    public MMFeedbacks mainPanelFinishFeedback;
    public MMFeedbacks playerStatScoreFeedback;
    public MMFeedbacks finalScoreFeedback;

    public float startDelay;
    public float skirmishGroupsBetween;
    public float playerStatDelay;
    public float finalScoreDelay;

    public SkirmishMovement[] skirmishMovements;

    public Sequence resultSequence;


    [Button]
    public void Use()
    {
        mainPanelGroup.alpha = 1;
        mainPanelGroup.gameObject.SetActive(true);

        DOTween.Kill(resultSequence);

        SetPanelFalse(false);
        skirmishMovements = skirmishGroups.GetComponentsInChildren<SkirmishMovement>(true);

        resultSequence = DOTween.Sequence();

        resultSequence.AppendInterval(startDelay);
        resultSequence.AppendCallback(SetActive_SkirmishGroup);

        for (int i = 1; i < skirmishMovements.Length; i++)
        {
            resultSequence.AppendCallback(skirmishMovements[i].Use);
            resultSequence.AppendInterval(skirmishGroupsBetween);
        }

        resultSequence.AppendInterval(playerStatDelay);
        resultSequence.AppendCallback(SetActive_playerStatGroup);
        resultSequence.AppendCallback(playerStatScoreFeedback.PlayFeedbacks);

        resultSequence.AppendInterval(finalScoreDelay);
        resultSequence.AppendCallback(SetActive_FinalScore);
        resultSequence.AppendCallback(finalScoreFeedback.PlayFeedbacks);

        resultSequence.Play();
    }

    [Button]
    public void Finish()
    {
        mainPanelFinishFeedback.PlayFeedbacks();
    }


    private void SetPanelFalse(bool _value)
    {
        skirmishGroups.gameObject.SetActive(_value);
        playerStatGroups.gameObject.SetActive(_value);
        finalScore.gameObject.SetActive(_value);
    }

    void SetActive_SkirmishGroup()
    {
        skirmishGroups.gameObject.SetActive(true);
    }

    void SetActive_playerStatGroup()
    {
        playerStatGroups.gameObject.SetActive(true);
    }

    void SetActive_FinalScore()
    {
        finalScore.gameObject.SetActive(true);
    }

}
