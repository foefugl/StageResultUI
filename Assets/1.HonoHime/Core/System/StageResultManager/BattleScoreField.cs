using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HonoHime.ScoreRankSystem 
{
    [System.Serializable]
    public struct ScoreReport
    {
        public string ID;
        public int score;
        public int clearTimeSecond;
        public int getDamageCount;
        public ScoreRank scoreRank;
        public int scoreRankBonus;
        public ScoreRank clearTimeScoreRank;
        public int clearTimeScoreRankBonus;
        public ScoreRank getDamageCountRank;
        public int getDamageCountRankBonus;
        public int totalScore;
        public ScoreRank totalScoreRank;
        public BattleEvaluateData battleEvaluateData;
    }
    public enum ScoreRank
    {
        S = 0,
        A = 1,
        B = 2,
        C = 3,
        D = 4,
    }

    public class EffectWeightData
    {
        public string name;
        public float curTime;
        public float totalTime;
        public float weight;
    }
}
