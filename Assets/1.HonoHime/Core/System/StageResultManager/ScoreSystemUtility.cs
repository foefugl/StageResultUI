
namespace HonoHime.ScoreRankSystem
{
    [System.Serializable]
    public class TotalScoreRankInfos
    {
        public TotalScoreRankInfo S;
        public TotalScoreRankInfo A;
        public TotalScoreRankInfo B;
        public TotalScoreRankInfo C;

        [System.Serializable]
        public class TotalScoreRankInfo
        {
            public int Score;
        }
    }
    [System.Serializable]
    public class ScoreRankInfos
    {
        public ScoreRankInfo S;
        public ScoreRankInfo A;
        public ScoreRankInfo B;
        public ScoreRankInfo C;

        [System.Serializable]
        public class ScoreRankInfo
        {
            public int Score;
        }
    }
    [System.Serializable]
    public class RankClearTimeSetting
    {
        public ClearTimeRankInfo S;
        public ClearTimeRankInfo A;
        public ClearTimeRankInfo B;
        public ClearTimeRankInfo C;

        [System.Serializable]
        public class ClearTimeRankInfo
        {
            public int ClearTime;
        }
    }
    [System.Serializable]
    public class GetDamageCountRankSetting
    {
        public GetDamageCountRankInfo S;
        public GetDamageCountRankInfo A;
        public GetDamageCountRankInfo B;
        public GetDamageCountRankInfo C;

        [System.Serializable]
        public class GetDamageCountRankInfo
        {
            public int getDamageCount;
        }
    }
}
