using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine.UI;

public class StageScrollMovement : MonoBehaviour
{
    public CanvasGroup canvasGroup;
    public RectTransform maskTrans;
    public RectTransform scroll;
    public Vector3 scrollLocalPos;

    public float scrollShowPosX;
    public float scrollShowDuration;
    public float scrollOpenDelay;
    public float scrollOpenDuration;
    public float fadeOutDuration;

    public Sequence sequence;

    private void Awake()
    {
        scrollLocalPos = scroll.localPosition;

        scroll.gameObject.SetActive(false);

        canvasGroup.alpha = 1f;
        scroll.anchoredPosition = new Vector2(0, scroll.anchoredPosition.y);
        maskTrans.rect.Set(maskTrans.rect.x, maskTrans.rect.y, 0, maskTrans.rect.height);
    }


    [Button]
    public void Use()
    {
        DOTween.Kill(sequence);

        scroll.anchoredPosition = new Vector2(0, scroll.anchoredPosition.y);
        maskTrans.sizeDelta = new Vector2(0, maskTrans.sizeDelta.y);

        canvasGroup.alpha = 1f;
        scroll.gameObject.SetActive(true);
        maskTrans.gameObject.SetActive(true);


        sequence = DOTween.Sequence();

        sequence.Append(scroll.DOAnchorPosX(scrollShowPosX, scrollShowDuration));
        sequence.AppendInterval(scrollOpenDelay);
        sequence.Append(maskTrans.DOSizeDelta(new Vector2(1920f, maskTrans.rect.height), scrollOpenDuration));
        sequence.Join(scroll.DOAnchorPosX(2300f, scrollOpenDuration));
    }

    [Button]
    public void Finish()
    {
        DOTween.Kill(canvasGroup);
        canvasGroup.DOFade(0f, fadeOutDuration);
    }
}
